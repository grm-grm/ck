#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "cklist.h"
#include "ckutil.h"

/* total number of unit tests */
#define TOTAL 2

#define CK_UNIT_TESTS                                         \
  X(ck_list_test, "Basic cklist test")                        \
  X(ck_str_utils_test, "Test string utility functions")       \

void ck_list_test() {
  /* init and add */
  cklist *ckl = list_make_new();
  cklist *ckl2 = list_make_and_add("first");
  list_add(ckl2, "second");
  list_add(ckl, "first");
  list_add(ckl, "second");
  assert(strcmp(list_get_at(ckl, 0),
		list_get_at(ckl2, 0)) == 0);
  assert(strcmp(list_get_at(ckl, 1),
		list_get_at(ckl2, 1)) == 0);
  list_free(ckl2);

  /* exists */
  assert(list_exists(ckl, "first"));
  assert(list_exists(ckl, "second"));
  assert(!list_exists(ckl, "thrid"));
  list_free(ckl);
}

void ck_str_utils_test() {
  /* make_ck_config_name */
  char ck_conf_path[STR_M] = "";
  str_make_ck_config_name(ck_conf_path, "/test/path/.config", "emacs");
  assert(strcmp(ck_conf_path, "emacs/.config") == 0);

  /* str_is_empty */
  assert(str_is_empty(""));
  assert(str_is_empty("    "));
  assert(str_is_empty("\t"));

  /* swap ~ and home */
  char str[STR_M] = "";
  char str2[STR_M] = "";
  /* getenv("HOME") -> /home/user */
  char home[STR_S] = "/home/user";
  char testpath[STR_S] = "/home/user/file";
  assert(swap_home_with_tilde(str, testpath, home));
  assert(strcmp(str, "~/file") == 0);
  assert(swap_tilde_with_home(str2, str, home));
  assert(strcmp(str2, testpath) == 0);
  assert(swap_home_with_tilde(str, "/not/starting/with/home", home) == NULL);
  assert(swap_tilde_with_home(str2, "/not/starting/with/tilde", home) == NULL);
}

int main() {
  int count = 1;
#define X(TEST, DESC)				\
  printf("[%d/%d] %s\n", count++, TOTAL, DESC); \
  TEST();					\
  printf("=> Passed\n\n");
  CK_UNIT_TESTS
#undef X
}

