#!/bin/bash

init list

# add configs to ck
path1=$BIN/test1.conf
path2=$BIN/test2.conf
path3=$BIN/test3.conf

add_config prog1 $path1
add_config prog2 $path2
add_config prog3 $path3

for i in $($BIN/ck -c $BIN list paths); do
    if [[ "$i" != "$path1" ]] && [[ "$i" != "$path2" ]] && [[ "$i" != "$path3" ]]; then
	err "Listing paths"
    fi
done

for i in $($BIN/ck -c $BIN list programs); do
    if [[ "$i" != "prog1" ]] && [[ "$i" != "prog2" ]] && [[ "$i" != "prog3" ]]; then
	err "Listing programs"
    fi
done

clear_tests
echo -e $PASS
