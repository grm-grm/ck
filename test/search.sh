#!/bin/bash

init search

# setup test configs
path1=$BIN/test1.conf
path2=$BIN/test2.conf
path3=$BIN/test3.conf
path4=$BIN/test4.conf

add_config prog1 $path1 "search-term"
add_config prog1 $path2 "search-term"
add_config prog2 $path3 "search-term"
add_config prog3 $path4 "search-term"

# search
if [[ $($BIN/ck -c $BIN search search-term | wc -l) != 4 ]]; then
  err "search failed."
fi

clear_tests
echo -e $PASS
