#!/bin/bash

running init

mkdir -p $TEST_LOCATION/vc
mkdir $TEST_LOCATION/sec

exec $BIN/ck -c $BIN init $TEST_LOCATION/vc $TEST_LOCATION/sec >&${V} &
wait $!

if [ $? -ne 0 ]; then
    err "ck crashed."
fi

if [ ! -f $BIN/ckrc ]; then
    err "Config file not created."
fi

if [ ! -f $BIN/ckdb ]; then
    err "DB file not created."
fi

clear_tests > /dev/null 2>&1
echo -e $PASS
