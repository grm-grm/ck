#!/bin/bash

init restore

# setup test configs
path1=$BIN/test1.conf
path2=$BIN/test2.conf
path3=$BIN/test3.conf
path4=$BIN/test4.conf

add_config prog1 $path1
add_config prog1 $path2

add_config prog2 $path3
add_config prog2 $path4

# delete prog1 links
rm "$path1" "$path2"

# restore them
exec $BIN/ck -c $BIN restore prog1 >&${V} &
wait $!

for i in $($BIN/ck -c $BIN list -p prog1); do
    if [[ ! -L "$i" ]]; then
       err "Couldn't restore path $i from -p prog1"
       exit 1
    fi
done

## delete all links
rm "$path1" "$path2" "$path3" "$path4"

# restore all
exec $BIN/ck -c $BIN restore --all >&${V} &
wait $!

for i in $($BIN/ck -c $BIN list paths); do
    if [[ ! -L "$i" ]]; then
       err "Couldn't restore path $i from all"
       exit 1
    fi
done

clear_tests
echo -e $PASS
