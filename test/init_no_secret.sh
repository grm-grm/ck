running init_no_secret

mkdir -p $TEST_LOCATION/vc

exec $BIN/ck -c $BIN init $TEST_LOCATION/vc >&${V} &
wait $!

if [ $? -ne 0 ]; then
    err "ck crashed."
fi

if [ ! -f $BIN/ckrc ]; then
    err "Config file not created."
fi

if [ "$(sqlite3 $BIN/ckdb "select VAL from CONTEXT where KEY = 'secret_enabled';")" != "0" ]; then
    err "secret_enabled should be 0."
fi

# setup test configs
echo "Test 1" > $BIN/test1.conf

exec $BIN/ck -c $BIN add prog1 test1.conf -s >&${V} &
wait $!

if [ $? -eq 0 ]; then
    err "Adding secret should fail"
fi

exec $BIN/ck -c $BIN add prog1 test1.conf >&${V} &
wait $!

if [ $? -ne 0 ]; then
    err "ck crashed."
fi

clear_tests > /dev/null 2>&1
echo -e $PASS
