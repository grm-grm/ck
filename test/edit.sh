#!/bin/bash

init edit

# add configs to ck
path1=$BIN/test1.conf
path2=$BIN/test2.conf
path3=$BIN/test3.conf
path4=$BIN/test4.conf
path5=$BIN/test5.conf

add_config prog1 $path1 "" -p
add_config prog1 $path2

add_config prog2 $path3
add_config prog2 $path4

add_config prog3 $path5

# edit primary
exec $BIN/ck -c $BIN e prog1 --command ":" > __test_file &
wait $!

TEST_STR=$(cat __test_file)
EXPECTED_STR="Running: : $(realpath $path1)"

echo "Expected: $EXPECTED_STR" >&${V}
echo "  Actual: $TEST_STR" >&${V}

if [[ "$TEST_STR" != "$EXPECTED_STR" ]]; then
    err "Worng edit."
    exit 1;
fi

# edit specific
exec $BIN/ck -c $BIN e prog1 test2.conf --command ":" > __test_file &
wait $!

echo "Expected: $EXPECTED_STR" >&${V}
echo "  Actual: $TEST_STR" >&${V}

TEST_STR=$(cat __test_file)
EXPECTED_STR="Running: : $(realpath $path2)"

if [[ "$TEST_STR" != "$EXPECTED_STR" ]]; then
    err "Worng edit."
    exit 1;
fi

# Ambiguous config
exec $BIN/ck -c $BIN e prog2 --command ":" > __test_file &
wait $!

echo "Expected: $EXPECTED_STR" >&${V}
echo "  Actual: $TEST_STR" >&${V}

TEST_STR=$(head -1 __test_file)
EXPECTED_STR="Ambiguous config. Please type the config name after the program."

if [[ "$TEST_STR" != "$EXPECTED_STR" ]]; then
    err "Worng edit."
    exit 1;
fi

# solo program
exec $BIN/ck -c $BIN e prog3 --command ":" > __test_file &
wait $!

echo "Expected: $EXPECTED_STR" >&${V}
echo "  Actual: $TEST_STR" >&${V}

TEST_STR=$(cat __test_file)
EXPECTED_STR="Running: : $(realpath $path5)"

if [[ "$TEST_STR" != "$EXPECTED_STR" ]]; then
    err "Worng edit."
    exit 1;
fi

rm __test_file

clear_tests
echo -e $PASS
