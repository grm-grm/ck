#!/bin/bash

init add

# setup test configs
echo "Test 1" > $BIN/test1.conf
echo "Test 2" > $BIN/test2.conf
echo "Test 3" > $BIN/test3.conf

function run_add {
    # add configs to ck
    exec $BIN/ck -c $BIN add $1 $BIN/$2 $3 $4 >&${V} &
    wait $!

    if [ $? -ne 0 ]; then
        err "ck crashed."
    fi

    # check db
    if [ "$(sqlite3 $BIN/ckdb "select name from program where name = '$1'")" != "$1" ]; then
        err "$1 is not in the db."
    fi

    if [ "$($BIN/ck -c $BIN ls -p $1)" != "$BIN/$2" ]; then
        err "$2 is not in the db."
    fi

    # check files
    FOLDER=vc
    if [[ "$3" = "-s" || "$4" = "-s" ]]; then
        FOLDER=sec
    fi

    if [ ! -f $TEST_LOCATION/$FOLDER/$1/$2 ]; then
        err "$2 No move (add $3 $4)"
    fi

    if [ ! -L $BIN/$2 ]; then
        err "$2 No symlink (add $3 $4)"
    fi  
}

run_add prog1 test1.conf -p
run_add prog2 test2.conf -s
run_add prog3 test3.conf -p -s

clear_tests
echo -e $PASS
