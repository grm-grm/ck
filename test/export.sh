#!/bin/bash

init export

# add configs to ck
path1=$BIN/test1.conf
path2=$BIN/test2.conf
path3=$BIN/test3.conf
path4=$BIN/test4.conf
path5=$BIN/test5.conf

add_config prog1 $path1
add_config prog1 $path2
add_config prog2 $path3
add_config prog2 $path4
add_config prog3 $path5

# export
exec $BIN/ck -c $BIN export >&${V} &
wait $!

if [ ! -f $BIN/ck.tar.gz ]; then
    err "tar.gz was not created"
fi

rm $BIN/ck.tar.gz
clear_tests
echo -e $PASS
