#!/bin/bash

init delete

# setup test configs
path1=$BIN/test1.conf
path2=$BIN/test2.conf
path3=$BIN/test3.conf
path4=$BIN/test4.conf

add_config prog1 $path1
add_config prog1 $path2

add_config prog2 $path3
add_config prog2 $path4

# delete path1
exec $BIN/ck -c $BIN del prog1 `basename $path1` >&${V} &
wait $!

for i in $($BIN/ck -c $BIN list paths); do
    if [[ "$i" == "$path1" ]]; then
       err "Couldn't delete path."
       exit 1
    fi
done

# delete path2 (also prog1)
exec $BIN/ck -c $BIN del prog1 `basename $path2` >&${V} &
wait $!

for i in $($BIN/ck -c $BIN list programs); do
    if [[ "$i" == "prog1" ]]; then
       err "Removing all configs should delete the correspondig program."
    fi
done

# delete prog2
exec $BIN/ck -c $BIN del prog2 >&${V} &
wait $!

for i in $($BIN/ck -c $BIN list paths); do
    if [[ "$i" == "$path3" ]] || [[ "$i" == "$path4" ]]; then
       err "Removing the program should delete all it's configs."
    fi
done

for i in $($BIN/ck -c $BIN list programs); do
    if [[ "$i" == "prog1" ]]; then
       err "Couldn't remove program."
    fi
done

clear_tests
echo -e $PASS
