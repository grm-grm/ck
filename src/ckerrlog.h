/* ckerrlog.h - Error report and logging for ck ------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * The error reporting and logging infrastructure used across ck.
 * This file defines the basic macros other functions should call to
 * report problems or help the user.
 *
 * All macros use printf like syntax to format the resutling strings.
 * There is no need for newline (`\n`) it is automatically appended to
 * each string.
 *
 * ERRLOG(component-name)
 *   Should be called once in every .c file defining the component name
 *   to be used with error reporting and logging.
 *
 * LOG("format-string", ...)
 *   Add format-string to the log. The component name is prepended.
 *   The log can be saved to a logfile and/or printed at the end of
 *   the ck session provided the --verbose flag is active.
 *
 * ERR("format-string", ...)
 *   Reports the format-string at the moment of calling, prepended with
 *   Error and the component-name. Also adds the error to the log.
 *
 * HELP("format-string", ...)
 *   Print the format-string at the moment of calling, without adding
 *   any component name nor pushing the string to the log.
 *
 * -------------------------------------------------------------------------- */
#ifndef CKERRLOG_H
#define CKERRLOG_H

#include <stdarg.h>

#include "ckutil.h"

#define CK_STREAMS                              \
  X(err)                                        \
  X(log)                                        \
  X(logv)                                       \
  X(help)

void initialize_errlog(int argc, const char** argv);
void report_errlog();
void errlog_set_verbose(int level);
void errlog_make_logfile(char *logdir);

#define X(stream)                                                       \
  void ck## stream(const char *log, ...);                        \
  void ck## stream ##_with_delim(const char *d,const char *txt, ...);   \
  void report_## stream();
CK_STREAMS
#undef X

/**********/
/* Macros */
/**********/

/* define the component's name */
#define ERRLOG(_COMPONENT)                              \
  static const char COMPONENT[STR_S] = #_COMPONENT

#define ERR(...)                                          \
  ckerr_with_delim("\n", "Error in [%s]:", COMPONENT);    \
  ckerr(__VA_ARGS__);                                     \
  cklog_with_delim(" ", "ERROR: [%s]", COMPONENT);        \
  cklog(__VA_ARGS__);                                     \
  report_err();

#define sERR(...)                                         \
  ckerr(__VA_ARGS__);                                     \
  cklog_with_delim(" ", "ERROR: [%s]", COMPONENT);        \
  cklog(__VA_ARGS__);                                     \
  report_err();                                 

/* Print help message */
#define HELP(...)                               \
  ckhelp(__VA_ARGS__);                          \
  report_help();

/* Log event */
#define LOG(...)                                   \
  cklog_with_delim(" ", "[%s]", COMPONENT);        \
  cklog(__VA_ARGS__);

/* Print help message & log it */
#define hLOG(...)                               \
  HELP(__VA_ARGS__);                            \
  LOG(__VA_ARGS__);

#define LOG_V(...)
#endif /* CKERRLOG_H */
