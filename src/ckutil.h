/* ckutil.h - utility functions for ck ---------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * Utility funtions and global constants.
 *
 * -------------------------------------------------------------------------- */
#ifndef CKUTIL_H
#define CKUTIL_H

/*******************/
/* global includes */
/*******************/

#include <stdio.h>
#include <limits.h>
#include <stdlib.h>
#include <string.h>

#define PRINT_ERR(ERROR)                        \
  printf("--[ Error ]--\n%s\n", ERROR);

#define UNUSED(x) (void)(x)

/********************/
/* global constants */
/********************/

/* String length - Small */
#define STR_S 30

/* String length -Medium */
#define STR_M 100

/* String length - Large */
#define STR_L 400

/********************/
/* string functions */
/********************/

/* Create the config name to be used when storing a new config to 
 * the VC or SCRT dir */
void str_make_ck_config_name(char *ret, const char *path,
                             const char *progName);

/* Joins the two strings into ret, with a '/' in between */
void str_join_dirname_with_basename(char *ret, const char *path,
                                    const char *progName);

/* Returns 1 if str contains only whitespace, or nothing, 
 * else returns 0. */
int str_is_empty(const char *s);

char *swap_tilde_with_home(char *ret, const char *s, const char *home);
char *swap_home_with_tilde(char *ret, const char *s, const char *home);

/*********************/
/* utility functions */
/*********************/

/* Returns 1 if path is a directory, else returns 0. */
int util_is_dir(const char *path);

/* Returns 1 if file(or dir) exists, else returns 0.
 * Pass a char array in absPath to get the absolute path
 * of the file it it exists. Pass NULL if no need. */
int util_file_exists(const char *path, char *absPath);

/* Returns 1 if file(or dir) is readable and writable,
 * else returns 0. */
int util_is_file_rw(const char *path);

/* Returns 1 if file is link 0 otherwise */
int util_is_file_link(const char *path);

/* Wrapper around mkdir with 0775 permissions */
void util_mkdir(const char *path);

/* Moves file from path, to destination
 * keeping the same permissions.
 * Only deletes original on successful copy */
int util_move_file(const char *path, const char* dest);

/* Wrapper around symlink() */
int util_symlink_file(const char *path, const char* dest);

/* Chnage owner and group of `new` and make it like `old`*/
int util_own_grp_copy(const char *new, const char* old);

int util_is_link_owned_by_root(const char *link);

/* yes is 1, no is 0 */
int y_or_n_prompt();

void decorate_entry(char *entry, int secret, int primary, const char *path);
#endif // CKUTIL_H
