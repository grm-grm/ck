/* queries.h - Database stuff for ck -----------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 * 
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * SQlite tables and columns.
 * Form query functions.
 *
 * -------------------------------------------------------------------------- */
#ifndef DBHELPER_H
#define DBHELPER_H

#include <sqlite3.h>

/********************/
/* sqlite constants */
/********************/
#define TBL_PROGRAM "PROGRAM"
#define TBL_CONFIG "CONFIG"
#define TBL_REL "REL"
#define TBL_CTX "CONTEXT"

#define COL_PROGRAM_ID "ID"
#define COL_PROGRAM_NAME "NAME"

#define COL_CONFIG_ID "ID"
#define COL_CONFIG_PATH "PATH"
#define COL_CONFIG_SECRET "SECRET"
#define COL_CONFIG_PRIMARY "PRIME"

#define COL_REL_PROGRAM_ID "PID"
#define COL_REL_CONFIG_ID "CID"

#define COL_CTX_KEY "KEY"
#define COL_CTX_VAL "VAL"

#define KEY_CTX_SECRET "secret_enabled"

#define __BEGIN_TRANSACTION__ sqlite3_exec(db->ptr, "BEGIN TRANSACTION;", NULL, NULL, NULL);
#define __END_TRANSACTION__ sqlite3_exec(db->ptr, "END TRANSACTION;", NULL, NULL, NULL);

/****************/
/* form queries */
/****************/

void dbh_fq_make_tables(char *query);
void dbh_fq_insert_program(char *query);
void dbh_fq_insert_config(char *query);
void dbh_fq_select_id_from(char *query, const char* tableName);
void dbh_fq_select_all_tables(char *query);
void dhb_fq_insert_relationship(char *query);
void dhb_fq_find_program(char *query);
void dhb_fq_find_config(char *query);
void dhb_fq_find_relationship(char *query);
void dbh_fq_select_paths_with_attributes(char *query);
void dbh_fq_select_programs(char *query);
void dbh_fq_select_from_joined_eq(char *query, const char *selection, const char* condition);
void dbh_fq_select_from_joined_like(char *query, const char *selection, const char* condition);
void dbh_fq_delete_x_from_y(char *query, const char *x, const char *y);
void dbh_fq_count_program_relations(char *query);
void dbh_fq_get_pid_from_cid(char *query);
void dbh_fq_secret_enabled(char *query);
void dbh_fq_set_secret_enabled(char *query);
#endif /* DBHELPER_H */
