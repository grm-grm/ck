/* clparser.h - Command line parser for ck -----------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * The code here and in clparser.c is responsible for parsing
 * the user's input from the command line and return a struct
 * of the user's options ready to be handled by the rest of the 
 * procedures.
 *
 * -------------------------------------------------------------------------- */
#ifndef CLPARSER_H
#define CLPARSER_H

#include "cklist.h"

/* NAME   |   MIN ACCEPTED ARGS | MAX ACCEPTED ARGS */
#define CK_ACTIONS             \
  X(INIT, 1 , 2)               \
  X(ADD, 2, 4)                 \
  X(DEL, 1, 2)                 \
  X(EDIT, 1, 5)                \
  X(LIST, 1, 6)                \
  X(SEARCH, 1, 1)              \
  X(RESTORE, 1, 1)             \
  X(EXPORT, 0, 0)              \
  X(HELP, 1, 1)

enum ParseErrors {
  PERR_NOERR = 0,
#define X(ACTION, MIN, MAX)                               \
  PERR_##ACTION##_WRONG,
  CK_ACTIONS
#undef X
  PERR_UNKNOWN_ACTION,
};
typedef enum ParseErrors ParseError;

enum CkActions {
  CK_WRONG_ACTION,
#define X(ACTION, MIN, MAX)                               \
  CKA_##ACTION,
  CK_ACTIONS
#undef X
};
typedef enum CkActions CkAction;

typedef struct UserOptions UserOpt;
struct UserOptions {
  int version;
  ParseError err;
  CkAction action;
  char *confDir;
  cklist *args;
};


/* Parse cli args, fill UserOpt struct */
int parse_action(int argc, const char **argv, UserOpt *opt);
CkAction parser_get_action(const char *name, char *actionName);
char * get_possible_action_strings(char *dest, CkAction ckAction);
void free_user_opt(UserOpt *opt);

#endif // CLPARSER_H
