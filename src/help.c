/* help.c - the help action --------------------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -------------------------------------------------------------------------- */
#include "dblayer.h"
#include "ckerrlog.h"

ERRLOG(action);

static void print_conf_help(void) {
  HELP("ck [-v|--verbose] [-c|--config DIR] action [...]");
}

static void print_verbose_help(void) {
  HELP("ck [-v|--verbose] [-c|--config DIR] action [...]");
}

int run_HELP(UserOpt *opt, Conf *conf) {
  UNUSED(conf);
  char tmp[STR_M] = "";
  if (strcmp(list_get(opt->args), "config") == 0) {
    print_conf_help();
    return 0;
  }

  if (strcmp(list_get(opt->args), "verbose") == 0) {
    print_verbose_help();
    return 0;
  }

  switch(parser_get_action(list_get(opt->args), NULL)) {
#define X(ACTION, MIN, MAX)                                                       \
    case CKA_##ACTION:                                                  \
      HELP("%s", get_possible_action_strings(tmp, CKA_##ACTION));       \
      print_##ACTION##_help();                                          \
      return 0;
    CK_ACTIONS
#undef X
  default:
    ERR("Unknown action: %s", list_get(opt->args));
  }
  return -1;
}

void print_HELP_help() {
  HELP("ck help action");
}
