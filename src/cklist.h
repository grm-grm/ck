/* cklist.h - List data structure for ck -------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * Data structure to hold a dynamic list of strings used accross ck.
 *
 * -------------------------------------------------------------------------- */
#ifndef CKLIST_H
#define CKLIST_H

struct cklist_st {
  unsigned int size;
  unsigned int pos;
  char **arr;
};
typedef struct cklist_st cklist;

cklist* list_make_new();
void list_add(cklist *ckl, const char *str);
cklist* list_make_and_add(const char *str);
void list_rewind(cklist *ckl);

int list_next(cklist *ckl);
char* list_get(cklist *ckl);
char* list_get_at(cklist *ckl, unsigned int pos);

unsigned int list_size(cklist *ckl);

/* rewinds */
cklist* list_duplicate(cklist *ckl);
/* rewinds */
cklist* list_move(cklist *ckl);
/* rewinds
 * copy from index (>=) to the end */
cklist* list_copy_from(cklist *ckl, unsigned int index);
/* rewinds 
 * copy from the start until (<) index*/
cklist* list_copy_until(cklist *ckl, unsigned int index);

/* rewinds 
 * copy from (>=) until (<) */
cklist* list_copy_part(cklist *ckl ,unsigned int from, unsigned int until);

/* return 1 if str exists in the list, 0 otherwise */
int list_exists(cklist *ckl, const char *str);

/* rewinds */
void list_print_lisp(cklist *ckl);
void list_print_python(cklist *ckl);
void list_print(cklist *ckl);
void list_print_concat(cklist *ckl);

int list_write_to_file(cklist *ckl, char *path, int append);

/* Deallocate resources */
void list_free(cklist *ckl);

#endif /* CKLIST_H */
