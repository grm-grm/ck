/* actions.h - ck actions ----------------------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * The various structs actions use to keep track of their
 * specific options and the run_ACTION routines.
 *
 * -------------------------------------------------------------------------- */
#ifndef ACTIONS_H
#define ACTIONS_H

#include "ckutil.h"
#include "confparser.h"

#define X(ACTION, MIN, MAX)                               \
  int run_##ACTION(UserOpt *, Conf *);
CK_ACTIONS
#undef X

enum AddOptErrors {
  ADD_NO_ERR = 0,
  ADD_ERR_WRONG_CONFIG,
  ADD_ERR_LINK_CONFIG,
  ADD_ERR_NO_SECRET,
  ADD_ERR_WRONG_FLAGS
};
typedef enum AddOptErrors AddOptErr;

struct AddOptions {
  char *progName;
  char confPath[STR_L];
  int secret;
  int prime;
  AddOptErr err;
};
typedef struct AddOptions AddOpt;

enum ListTypes {
  LT_PATH,
  LT_PROGRAM,
  LT_TREE,
  LT_CKCONF,
  LT_PROG_CONFS
};
typedef enum ListTypes ListType;

enum ListShowTypes {
   LST_PLAIN,
   LST_LISP,
   LST_PYTHON,
};
typedef enum ListShowTypes ListShowType;

struct ListOptions {
  ListType _lt;
  ListShowType _lst;
  char *pName;
  int attr;
  int bName;
  int err;
};
typedef struct ListOptions ListOpt;

struct EditOptions {
  char *pName;
  char *cBasename;
  char *editor;
  char *cmd;
  int sudo;
  int err;
};
typedef struct EditOptions EditOpt;

/**************/
/* PRINT HELP */
/**************/
#define X(ACTION, MIN, MAX)                        \
  void print_##ACTION##_help(void);
CK_ACTIONS
#undef X
#endif /* ACTIONS_H */
