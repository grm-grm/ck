/* dblayer.h - Database layer for ck -----------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 * 
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * Give access to the database.
 *
 * -------------------------------------------------------------------------- */
#ifndef DBLAYER_H
#define DBLAYER_H

#include <sqlite3.h>

#include "actions.h"
#include "queries.h"

enum SqlErrors {
  SQL_NO_ERR = 0,
  SQL_ERR_NO_DB_FILE,
  SQL_ERR_NO_TABLES,
  SQL_ERR_SQLITE,
  SQL_CONFIG_PATH_EXISTS,
  SQL_ERR_PRIMARY_REDEFINITION
};
typedef enum SqlErrors SqlError;

typedef struct DBstruct DB;
struct DBstruct {
  sqlite3 *ptr;
  SqlError error;
};

/* Open the db file. On fail return null pointer to db->ptr
 * and the corresponding SQL error (NO_DB_FILE | NO_TABLES)*/
int open_DB(DB *db, const UserOpt *opt);
void close_DB(DB *db);
int db_exists(const UserOpt *opt);


/**********************************/
int program_exists(DB *db, const char *pName);
int get_program_paths(DB *db, cklist *ckl, const char* pName, int bName, int attr, const char *home);
int get_config_number(DB *db, char *pName);
int get_program_relations(DB *db, int pid);
/* Returns the path of the found config via *ret */
int program_has_primary_config(DB *db, const int pid, char *ret, int *sec);

/* Returns -2 or error, -1 if program doesn't exist
 * else the program ID */
int get_config_id(DB *db, const char* path);

/* Returns -2 or error, -1 if program doesn't exist
 * else the config ID */
int get_program_id(DB *db, const char* name);

int get_pid_from_cid(DB *db, int cid);

void print_suggested_configs(DB *db, const char *pName);

int list_get_paths(DB *db, cklist *ckl, int bName, int attr, const char *home);
int list_get_programs(DB *db, cklist *ckl);

int secret_enabled(DB *db);
#endif /* DBLAYER_H */
