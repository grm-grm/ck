/* export.c - the export action  ---------------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -------------------------------------------------------------------------- */
#include <libgen.h>

#include "dblayer.h"
#include "ckerrlog.h"

ERRLOG(export);

int run_EXPORT(UserOpt *opt, Conf *conf) {
  if (system("which tar > /dev/null 2>&1") != 0) {
    ERR("No tar avaliable. Please make sure you have tar installed.");
    return -1;
  }

  char cmd[STR_L] = "tar -zcvf ck.tar.gz";
  if(conf->scrt_dir) {
    strcat(cmd, " -C ");
    strcat(cmd, conf->scrt_dir);
    strcat(cmd, " ../");
    strcat(cmd, basename(conf->scrt_dir));
  }
  strcat(cmd, " -C ");
  strcat(cmd, conf->vc_dir);
  strcat(cmd, " ../");
  strcat(cmd, basename(conf->vc_dir));
  strcat(cmd, " -C ");
  strcat(cmd, opt->confDir);
  strcat(cmd, " ckrc -C ");
  strcat(cmd, opt->confDir);
  strcat(cmd, " ckdb");

  hLOG("Running: %s", cmd);
  strcat(cmd, " > /dev/null 2>&1");
  return system(cmd);
}

void print_EXPORT_help() {
  HELP("ck export");
}
