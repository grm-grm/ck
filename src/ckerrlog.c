/* ckerrlog.c - Error report and logging for ck ------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -------------------------------------------------------------------------- */
#include <time.h>

#include "ckerrlog.h"
#include "cklist.h"

ERRLOG(logger);

static int loglvl;
static char logfile[STR_L] = "";
static char buf[STR_M] = "";

#define X(stream) static cklist *stream;
CK_STREAMS
#undef X

static char *get_time() {
  time_t rawtime;
  struct tm * timeinfo;
  time (&rawtime);
  timeinfo = localtime (&rawtime);
  strftime (buf,80,"[%c]",timeinfo);
  return buf;
}

void log_command(int argc,const char* argv[]) {
  char tmp[STR_L] = "";
  for(int i = 0; i < argc; i++) {
    strcat(tmp, argv[i]);
    strcat(tmp, " ");
  }
  LOG("Command issued: %s", tmp);
}

#define X(stream)                                               \
  void add_ ## stream ## _with_delim(const char *delim,         \
                                     const char *txt,           \
                                     va_list args) {            \
    char tmp[STR_L] = "";                                       \
    vsprintf(tmp, txt, args);                                   \
    if (stream) {                                               \
      list_add(stream, tmp);                                    \
      list_add(stream, delim);                                  \
      return;                                                   \
    }                                                           \
    stream = list_make_and_add(tmp);                            \
    list_add(stream, delim);                                    \
  }
CK_STREAMS
#undef X

#define X(stream)                                             \
  void ck## stream(const char *txt, ...) {                    \
    va_list args;                                             \
    va_start(args, txt);                                      \
    add_## stream ##_with_delim("\n", txt, args);             \
    va_end(args);                                             \
  }
CK_STREAMS
#undef X

#define X(stream)                                                       \
  void ck## stream ##_with_delim(const char *d, const char *txt, ...) { \
    va_list args;                                                       \
    va_start(args, txt);                                                \
    add_## stream ##_with_delim(d, txt, args);                          \
    va_end(args);                                                       \
  }
CK_STREAMS
#undef X

#define X(stream)                               \
  void reset_## stream() {                      \
    list_free(stream);                          \
    stream = NULL;                              \
  }
CK_STREAMS
#undef X

#define X(stream)                                \
  void report_## stream() {                      \
    list_print_concat(stream);                   \
    reset_## stream();                           \
  }
CK_STREAMS
#undef X

void report_errlog() {
  LOG("------- end -------\n");
  if (!str_is_empty(logfile)) {
#define X(stream)                               \
  if (stream) {                                 \
    list_write_to_file(stream, logfile, 1);     \
  }
  CK_STREAMS
#undef X
    }

  if (!loglvl) {
#define X(stream)                               \
    if (stream) {                               \
      reset_##stream();                         \
    }
    CK_STREAMS
#undef X
      return;
  }
#define X(stream)                               \
  if (stream) {                                 \
    printf("%s:\n", #stream);                   \
    report_##stream();                          \
  }
  CK_STREAMS
#undef X
}

void ckerr_add_component(char *txt, ...) {
  va_list args;
  va_start(args, txt);
  add_err_with_delim(" ", txt, args);
  va_end(args);
}

void errlog_make_logfile(char *logdir) {
  if (!logdir) {
    return;
  }
  str_join_dirname_with_basename(logfile, logdir, "ck.log");
}

void errlog_set_verbose(int level) {
  loglvl = level;
}

void initialize_errlog(int argc, const char** argv) {
#define X(stream) stream = NULL;
  CK_STREAMS
#undef X
  loglvl = 0;
  LOG("%s Log session started", get_time());
  log_command(argc, argv);
}
