/* confparser.h - Configuration file parser for ck ---------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 * 
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * -----------------------------------------------------------------------------
 *
 * The code here and in confparser.c is responsible for parsing
 * the configuration and getting the values set there in the Conf struct
 *
 * -------------------------------------------------------------------------- */
#ifndef CONFPARSER_H
#define CONFPARSER_H

#include "clparser.h"

/* name | match str | desc | optional */
#define CONFIG_VARIABLES_TABLE                                              \
  X(vc_dir,   " version_control_dir = %s ", "Version Control directory", 0) \
  X(scrt_dir, " secret_dir = %s "         , "Secret directory",          1) \
  X(home_dir, " home_dir = %s "           , "Home directory",            0) \
  X(log_dir,  " log_dir = %s "            , "Log directory",             1)

enum ConfingVariables {
  CV_NO_VAL_OR_COMMENT,
  CV_WRONG_VAL,
#define X(var, str, name, optional) CV_##var,
  CONFIG_VARIABLES_TABLE
#undef X
};
typedef enum ConfingVariables ConfVar;

typedef struct ConfigValues Conf;
struct ConfigValues {
#define X(var, str, name, optional) char* var;
  CONFIG_VARIABLES_TABLE
#undef X
};

/* Parse the configuration file and fill the conf struct */
int config_file_parse(Conf *conf, UserOpt *opt);

void make_config_name(char * ret, const char *confPath);
int find_config(UserOpt *opt);
void initialize_conf(Conf *conf);
void free_conf(Conf *conf);
#endif // CONFPARSER_H
