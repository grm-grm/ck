/* ck.c - the main -----------------------------------------------------*- C -*-
 *
 * This file is part of ck, the config keeper
 *
 * -----------------------------------------------------------------------------
 *
 * Copyright (C) 2019  Anastasis Grammenos
 * GPLv3 (see LICENCE for the full notice)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -----------------------------------------------------------------------------
 *
 * This is the file where the main function lies.
 * It first parses the user options and config, then passes 
 * the results to the run_ACTION functions.
 *
 * -------------------------------------------------------------------------- */

#include "dblayer.h"
#include "ckerrlog.h"

ERRLOG(main);

int main(int argc, const char **argv) {
  UserOpt opt;
  Conf conf;
  int rc = -1;

  initialize_errlog(argc, argv);
  initialize_conf(&conf);

  /* get user opt */
  if (parse_action(argc, argv, &opt)) {
    if(opt.version) rc = 0;
    goto error;
  }
  /* If the action is init or help don't load the config, skip to running it*/
  if (opt.action != CKA_INIT &&
      opt.action != CKA_HELP) {
    /* If the db doesn't exist ck is not initialized in the config 
     * location specified in opt */
    if (!db_exists(&opt)) {
      ERR("ck is not initialized in %s.\nRun ck init first.", opt.confDir);
      goto error;
    }
    /* Finally parse the config file and exit on error */
    if (config_file_parse(&conf, &opt)) {
      goto error;
    }
  }

  /* Run action and print results */
  switch(opt.action) {
#define X(ACTION, MIN, MAX)            \
    case CKA_##ACTION:                 \
      rc = run_##ACTION(&opt, &conf);  \
      break;
    CK_ACTIONS
#undef X
  default:
    break;
  }
 error:
  free_user_opt(&opt);
  free_conf(&conf);
  report_errlog();
  return rc;
}
